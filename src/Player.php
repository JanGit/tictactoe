<?php
/**
 *
 */
class Player
{
  Private $player1;
  Private $player2;
  Private $currentplayer;

  function __Construct($player1, $player2){
    $this->player1 = $player1;
    $this->player2 = $player2;
    if(empty($this->currentplayer)){
        $this->currentplayer = $this->player2;
        $_SESSION['currPlayer'] = serialize($this->currentplayer);
      }
  }

  function getCurrentPlayer(){
    $this->currentplayer = unserialize($_SESSION['currPlayer']);
    if($this->currentplayer === $this->player1){
      $this->currentplayer = $this->player2;
      $_SESSION['currPlayer'] = serialize($this->currentplayer);
      return $this->player2;
    }
    else if ($this->currentplayer === $this->player2){
      $this->currentplayer = $this->player1;
      $_SESSION['currPlayer'] = serialize($this->currentplayer);
      return $this->player1;
    }
  }

  function getPlayer1()
  {
    return $this->player1;
  }
  function setPlayer1($player1)
  {
    $this->player1 = $player1;
  }
  function getPlayer2()
  {
    return $this->player2;
  }
  function setPlayer2($player2)
  {
    $this->player2 = $player2;
  }
}

 ?>
