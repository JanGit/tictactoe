
<?php
class Board
{
  private $board;

  function __Construct(){
    if(empty($_SESSION['board'])){
    $this->board = array(array("","",""),array("","",""),array("","",""),);
    $_SESSION['board'] = serialize($this->board);
    }
  }

  function getBoard(){
    return $this->board;
  }

  function setPlayerTurn($player){
    $this->board = unserialize($_SESSION['board']);
    for ($j=0; $j < count($this->board); $j++){
      for ($k=0; $k < count($this->board[0]); $k++) {
        if(isset($_GET['cell-'.$j.'-'.$k.''])){
          $this->board[$j][$k] = $player;
          $_SESSION['board'] = serialize($this->board);
        }
      }
    }
  }

  function resetBoard(){
    session_destroy ();
  }

  function buildBoard($playerturn)
  {
    echo '<table class="tic">'."\n";
    for ($i=0; $i < count($this->board); $i++) {

      echo '<tr>'."\n";

      for ($j=0; $j < count($this->board[$i]); $j++) {

        if ($this->board[$i][$j] === ""){

          echo "\t" . '<td><input type="submit" class="reset field" name="cell-'.$i.'-'.$j.'" Value="'.$playerturn.'"/></td>'."\n";

        }

        else {

          echo "\t".'<td><span class="color'.$this->board[$i][$j].'">'.$this->board[$i][$j].'</span></td>'."\n";
        }
      }
        echo '<tr>'."\n";
    }
     echo '<table>'."\n";
  }
}
 ?>
